#+OPTIONS: author:nil

* Shifter
As a Shifter, you have the Nature emotional attribute.  This attribute starts at B3.

When you first gain the Nature emotional attribute choose one of five abilities: Forte, Power, Speed, Agility, or Perception.  You choice reflects the idiom of your Primal Shifting.

You may spend a Persona for your Nature to help your chosen ability.

You may use your Nature instead of Health when making tests for recovery.  You may use your Nature instead of a Steel test, but if you your only response is “Stand and Roar.”

** Shifting towards Your Primal Self

You may also make a Nature test to /Shift towards Your Primal Self/.  Depending on your idiom, this shift has different effects.  This form of shifting requires 2 actions and may be scripted with as part of a Stance change.  When you succeed at shifting, you gain the benefit for the scene.

When you fail, you gain the benefit for one test or one exchange (whichever comes first) and tax yourself (see below).

While shifting you may ignore either a +1Ob or a -1D in wound penalties.

*For Forte:*

- Ob1 :: Gain 1D armor.
- Ob5 :: Gain 2D armor.
- Ob9 :: Gain 3D armor.

This armor does not fail.

*For Power:*

- Ob1 :: +1D for Power Tests
- Ob5 :: +2D for Power Tests
- Ob9 :: +3D for Power Tests

*For Speed:*

- Ob1 :: +2 Stride; +1D to Climbing
- Ob5 :: +4 Stride; +2D to Climbing
- Ob9 :: +6 Stride; +3D to Climbing

*For Agility:*

- Ob1 :: Fangs (Pow: 1, Add: 2, VA: -, WS: 2, shortest)
- Ob5 :: Sharpened Fangs (Pow: 2, Add: 2, VA: -, WS: 2, shortest)
- Ob9 :: Best fangs (Pow: 2, Add: 2, VA: 2, WS: X, shortest)

*For Perception:*

- Ob1 :: +1D for assesses, tracking, hunting, and off-setting penalties due to lighting.
- Ob5 :: +2D for assesses, tracking, hunting, and off-setting penalties due to lighting.
- Ob9 :: +3D for assesses, tracking, hunting, and off-setting penalties due to lighting.

** Tax on Nature

When you fail a Nature test, you temporarily tax your Nature.  Reduce your Nature by the degree of failure.  For each point below zero, you require an additional day to recover from tax.  Otherwise you require 2 hours per point of tax to recover.  Once recovered, remove your tax on Nature.

While taxed, you may not /Shift towards Your Primal Self/ nor may you spend Persona for your Nature to help your ability.
